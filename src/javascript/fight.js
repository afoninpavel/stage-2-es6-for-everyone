import Fighter from './fighter'
import FightsView from "./fightsView";
import View from './view';

const fight = (fighter1,fighter2) => {
  const createElement = new View().createElement;
  const fighterLeft = new Fighter(fighter1);
  const fighterRight = new Fighter(fighter2);

  const modalsElement = document.getElementById('fight');
  modalsElement.innerHTML = '';

  function createModal() {
    const fightsView = new FightsView([fighterLeft,fighterRight]);
    const fightsElement = fightsView.element;

    fight.counerRound = createElement({tagName: 'div', className: 'fight__title'});
    fight.counerRound.innerText = 'Round 1';

    modalsElement.append(fight.counerRound, fightsElement);
    modalsElement.style.visibility = 'visible';
  }

  function fightRound() {
    if (!getHit(fighterLeft, fighterRight)){
      winner(fighterLeft);
      return true;
    }
    if (!getHit(fighterRight, fighterLeft)){
      winner(fighterRight);
      return true;
    }
    return false
  }

  function getDamage(fighterFirst, fighterSecond){
    const getHitPower  = fighterFirst.getHitPower();
    const getBlockPower = fighterSecond.getBlockPower();

    const res = getHitPower - getBlockPower;
    return res > 0 ? res : 0;
  }


  function getHit(fighterFirst, fighterSecond){
    fighterSecond.health -= getDamage(fighterFirst, fighterSecond);

    updateModal(fighterSecond);
    return fighterSecond.health > 0
  }

  function updateModal(fighter){
    const progressElement =  document.getElementById('fight__health-' + fighter._id);
    progressElement.value = fighter.health;
  }

  function winner(fighter) {
    fight.fightWinner = createElement({tagName: 'div', className: 'fight__alert'});
    fight.fightWinner.innerText = 'Winner: ' + fighter.name;

    console.log('Yes, winner ' + fighter.name + ' and there are no errors here)');

    const body = document.getElementsByTagName('body')[0];
    body.append(fight.fightWinner);

    fight.fightWinner.style.visibility = 'visible';
  }

  function timer(ms) {
    return new Promise(res => setTimeout(res, ms));
  }

  async function startFight () {
    createModal();

    let roundCounter = 1;
    while (!fightRound()) {
      ++roundCounter;
      fight.counerRound.innerText = 'Round ' + roundCounter;
      await timer(300);
    }
  }

  startFight();



  return {fighterLeft, fighterRight}
};

export default fight
