import View from './view'
import FighterView from './fighterView'
import { fighterService } from './services/fightersService'
import fight from './fight';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.handleClickAdd = this.handleFighterAdd.bind(this);
    this.createFighters(fighters);
    this.createStartElement();
  }

  fightersDetailsMap = new Map();
  fightersSelectedMap = new Map();
  modalsElement = document.getElementById('modals');

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick, this.handleClickAdd);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

    handleFighterClick(event, fighter) {
        const _id = fighter._id;
        if (!this.isDetailsInfoFighter(_id)) {
            this.addDetailsInfoFighter(_id)
        } else {
            this.createModal(_id);
        }
    }

    isDetailsInfoFighter(_id) {
        return this.fightersDetailsMap.has(_id)
    }

    addDetailsInfoFighter(_id) {
        fighterService.getFighterDetails(_id)
            .then(
                fighter => {
                    this.fightersDetailsMap.set(_id, fighter);
                    this.createModal(_id);
                },
                error => console.log(error)
            );
    }
    createModal(_id) {
        this.modalsElement.style.visibility = 'hidden';
        this.modalsElement.innerHTML = '';

        const fighterInfo = this.fightersDetailsMap.get(_id);

        const modalsForm = this.createElement({tagName: 'form', className: 'modals__form'});

        // Create modals head, img + title
        const modalsInfo = this.createElement({tagName: 'div', className: 'modals__info'});
        const modalsImg = this.createElement({
            tagName: 'img',
            className: 'modals__img',
            attributes: {src: fighterInfo.source}
        });
        const modalsName = this.createElement({tagName: 'div', className: 'modals__name'});
        modalsName.innerText = fighterInfo.name;
        modalsInfo.append(modalsImg, modalsName);

        // Create modals body, name + value
        const createdModalsField = (name, value) => {
            const modalsLabel = this.createElement({tagName: 'label'});
            const modalsSpan = this.createElement({tagName: 'span'});
            modalsSpan.innerText = name;
            const modalsInput = this.createElement({
                tagName: 'input',
                attributes: {
                    type: 'number',
                    name: name,
                    placeholder: name,
                    id: name,
                    value: value,
                    required: 'required'
                }
            });
            modalsLabel.append(modalsSpan, modalsInput);
            return modalsLabel;
        };
        const modalsLabelAttack = createdModalsField('attack', fighterInfo.attack);
        const modalsLabelDefense = createdModalsField('defense', fighterInfo.defense);
        const modalsLabelHealth = createdModalsField('health', fighterInfo.health);

        // Create modals btn, save + close
        const modalsActions = this.createElement({tagName: 'div', className: 'modals__actions'});
        const modalsSave = this.createElement({tagName: 'button', className: 'btn', attributes: {type: 'submit'}});
        modalsSave.innerText = 'Save';
        const modalsClose = this.createElement({tagName: 'button', className: 'btn-red', attributes: {type: 'button'}});
        modalsClose.innerText = 'Close';
        modalsClose.addEventListener('click', event => this.closeModal(event), false);
        modalsActions.append(modalsSave, modalsClose);

        modalsForm.addEventListener('submit', event => this.editFighter(event, fighterInfo), false);

        modalsForm.append(modalsInfo,
            modalsLabelAttack,
            modalsLabelDefense,
            modalsLabelHealth,
            modalsActions);

        this.modalsElement.append(modalsForm);
        this.modalsElement.style.visibility = 'visible';
    }

    closeModal(event) {
        this.modalsElement.style.visibility = 'hidden';
        this.modalsElement.innerHTML = '';
    }

    editFighter(event, fighter) {
      const formElement = event.target;
      const updateFighterInfo = Object.assign({}, fighter);
      const attack = +formElement.attack.value;
      const defense = +formElement.defense.value;
      const health = +formElement.health.value;

      updateFighterInfo.attack = attack > 1 ? attack : 1;
      updateFighterInfo.defense = defense > 1 ? defense : 1;
      updateFighterInfo.health = health > 5 ? health : 1;

      this.fightersDetailsMap.set(fighter._id, updateFighterInfo);

      this.closeModal();
      event.preventDefault();
    }

    handleFighterAdd(event, fighter) {
      const fighterId = fighter._id;

      if(event.target.checked){
        if (this.fightersSelectedMap.size < 2) {
          if (!this.isDetailsInfoFighter(fighterId)) {
            this.addDetailsInfoFighter(fighterId);
          }
          this.fightersSelectedMap.set(fighterId, fighter);
        } else {
          event.preventDefault(); //  No more 2 player
        }
      } else {
        this.fightersSelectedMap.delete(fighterId);
      }

      return false
    }

    createStartElement(){
      this.button = this.createElement({tagName: 'button', className: 'btn-start', attributes: {type: 'button'}});
      this.button.innerText = 'Start fight';
      this.button.addEventListener('click', () => this.startFight(), false);
    }

    startFight(){
      if (this.fightersSelectedMap.size < 2) {
        alert('Select two fighters!!');
        return false
      }
      let collectionFighter = [];
      for(let id of this.fightersSelectedMap.keys()) {
        collectionFighter.push(this.fightersDetailsMap.get(id));
      }
      fight(collectionFighter[0], collectionFighter[1]);
    }
}

export default FightersView;