class View {
  element

  createElement ({tagName, className = '', attributes = {}}) {
    const element = document.createElement(tagName)
    if (className.length > 0) {
      element.classList.add(className)
    }
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]))

    return element
  }
}

export default View
