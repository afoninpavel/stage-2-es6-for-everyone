import View from './view';
import FightView from "./fightView";

class FightsView extends View {
  constructor(fighters) {
    super();

    this.createFighters(fighters);
  }

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FightView(fighter);
      return fighterView.element;
    });

    this.element = this.createElement({tagName: 'div', className: 'fight__list'});
    this.element.append(...fighterElements);
  }

}


export default FightsView;
