import View from './view';

class FighterView extends View {
  constructor(fighter, handleClick, handleClickAdd) {
    super();

    this.createFighter(fighter, handleClick, handleClickAdd);
  }

  createFighter(fighter, handleClick, handleClickAdd) {
    const {_id, name, source} = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const checkElement = this.createCheckbox(_id);

    checkElement.addEventListener('click', event => {
      event.stopPropagation();
      return handleClickAdd(event, fighter);
    }, true);

    this.element = this.createElement({tagName: 'div', className: 'fighter'});
    this.element.append(imageElement, nameElement, checkElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  createName(name) {
    const nameElement = this.createElement({tagName: 'span', className: 'name'});
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = {src: source};
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }

  createCheckbox(_id){
    const checkboxElement = this.createElement({tagName: 'label', className: 'fighter-check'});
    const checkboxSpan = this.createElement({tagName: 'span'});
    checkboxSpan.innerText = 'To choose';
    const checkboxInput = this.createElement({
      tagName: 'input',
      attributes: {
        type: 'checkbox',
        name: _id,
        id: _id,
      }
    });
    checkboxElement.append(checkboxInput, checkboxSpan);
    return checkboxElement;
  }
}

export default FighterView;