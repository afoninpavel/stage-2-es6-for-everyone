import View from './view';

class FightView extends View {
  constructor(fighter) {
    super();

    this.createFighter(fighter);
  }

  createFighter(fighter) {
    const {_id, name, source, health} = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const progressElement = this.createProgress(health, _id);

    this.element = this.createElement({tagName: 'div', className: 'fight__item'});
    this.element.append(imageElement, nameElement, progressElement);

  }

  createName(name) {
    const nameElement = this.createElement({tagName: 'div', className: 'fight__name'});
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const imgElement = this.createElement({tagName: 'div', className: 'fight__img'});
    const attributes = {src: source};
    const imagesElement = this.createElement({
      tagName: 'img',
      attributes
    });
    imgElement.append(imagesElement);

    return imgElement;
  }

  createProgress(health, _id){
    const progressElement = this.createElement({
      tagName: 'progress',
      className: 'fight__health',
      attributes: {
        id: 'fight__health-' + _id,
        value: health,
        max: health,
      }
    });
    return progressElement;
  }
}

export default FightView;