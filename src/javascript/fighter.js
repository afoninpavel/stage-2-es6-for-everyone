class Fighter {
  constructor(fighter){

    this._id = fighter._id;
    this.name = fighter.name;
    this.health = fighter.health;
    this.attack = fighter.attack;
    this.defense = fighter.defense;
    this.source = fighter.source;
  }

  random(){
    return Math.floor(1 + Math.random() * 2)
  }

  getHitPower(){
    let criticalHitChance = this.random();
    return this.attack * criticalHitChance
  }

  getBlockPower(){
    let dodgeChance = this.random();
    return this.defense * dodgeChance;
  }
}

export default Fighter;
